//This will drap a Sierpinski Gasket of 100000 points


#include <GL/glut.h>

struct point
{
	GLfloat x;
	GLfloat y;

	point():
		x(0),
		y(0)
		{}

	point(GLfloat x, GLfloat y):
		x(x),
		y(y)
		{}

	point midpoint(point p) {return point((x + p.x) / 2.0, (y + p.y) / 2.0);}
};

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	static point vertices[] = {point(0, 0), point(250, 500), point(500, 0)};

	static point p = vertices[0];
	glBegin(GL_POINTS);
	for (int i = 0; i < 25000; ++i)
	{
		p = p.midpoint(vertices[rand() % 3]);
		glVertex2f(p.x, p.y);
	}
	glColor3f(0, 0, 1.0);
	for (int i = 0; i < 25000; ++i)
	{
		p = p.midpoint(vertices[rand() % 3]);
		glVertex2f(p.x, p.y);
	}
	glColor3f(0, 1.0, 0);
	for (int i = 0; i < 25000; ++i)
	{
		p = p.midpoint(vertices[rand() % 3]);
		glVertex2f(p.x, p.y);
	}
	glColor3f(1.0, 0, 0);
	for (int i = 0; i < 25000; ++i)
	{
		p = p.midpoint(vertices[rand() % 3]);
		glVertex2f(p.x, p.y);
	}
	glEnd();
	glFlush();
}

void init()
{
	glClearColor(0.25, 0.0, 0.2, 1.0);
	glColor3f(0.6, 1.0, 0.0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 500.0, 0.0, 500.0, 0.0, 1.0);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(40, 40);
	glutCreateWindow("Sierpinski Triangle");
	glutDisplayFunc(display);
	init();
	glutMainLoop();
}
