//3-d Sierpinsk "triangle"
#include <GL/glut.h>

struct point
{
	GLfloat x;
	GLfloat y;
	GLfloat z;

	point():
		x(0),
		y(0),
		z(0)
		{}

	point(GLfloat x, GLfloat y, GLfloat z):
		x(x),
		y(y),
		z(z)
		{}

	point midpoint(point p) {return point((x + p.x)/2, (y + p.y)/2, (z + p.z)/2);}
};

void reshape(GLint w, GLint h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective the bigger the angle for fov the farther the camera has to back up
	gluPerspective(90.0, GLfloat(w)/GLfloat(h), 10.0, 1500.0);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void generateMorePoints()
{
	static point vertices[4] =
	{
		point(-250, -255, -200),
		point(-150, -225, -700),
		point(250, -225, -275),
		point(0, 450, -500)
	};

	static point lastPoint = vertices[0];

	glBegin(GL_POINTS);
	for(int i = 0; i <= 500; ++i)
	{
		lastPoint = lastPoint.midpoint(vertices[rand() %4]);
		GLfloat intensity = (700 + lastPoint.z) / 500;
		glColor3f(intensity, intensity, 0.25);
		glVertex3f(lastPoint.x, lastPoint.y, lastPoint.z);
	}
	glEnd();
	glFlush();
}

void init()
{
	glEnable(GL_DEPTH_TEST);
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0,0);
	glutCreateWindow("Sierpinski Tetrahedron");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(generateMorePoints);
	init();
	glutMainLoop();
}