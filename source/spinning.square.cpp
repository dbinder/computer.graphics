//Spinning  Square
#include <GL/glut.h>

static bool spinning = true;

static const int FPS = 60;

static GLfloat currentAngelOfRotation = 0.0;

void reshape(GLint w, GLint h)
{
	glViewport(0, 0, w, h);
	GLfloat aspect = static_cast<GLfloat> (w) / static_cast<GLfloat> (h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(w <= h)
	{
		//width is smaller got from -50 .. 50 in width
		glOrtho(-50.0, 50.0, -50.0/aspect, 50.0/aspect, -1.0, 1.0);
	}
	else
	{
		//height is smaller, to -50 .. 50 in height
		glOrtho(-50.0 * aspect, 50.0 * aspect, -50.0, 50.0, -1.0, 1.0);
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(currentAngelOfRotation, 0.0, 0.0, 1.0);
	glRectf(-25.0, -25.0, 25.0, 25.0);
	glFlush();
	glutSwapBuffers();
}

void timer(int v)
{
	if (spinning)
	{
		currentAngelOfRotation += 1.0;
		if(currentAngelOfRotation > 360.0)
		{
			currentAngelOfRotation -= 360.0;
		}
		glutPostRedisplay();
	}
	glutTimerFunc(1000/FPS, timer, v);
}

void mouse(int button, int state, int x, int y)
{
	switch(button)
	{
		case GLUT_LEFT_BUTTON:
			if (state == GLUT_DOWN)
			{
				spinning = true;
			}
			break;
		case GLUT_RIGHT_BUTTON:
			if (state == GLUT_DOWN)
			{
				spinning = false;
			}
			break;
		default:
			break;
	}
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowPosition(80, 80);
	glutInitWindowSize(800, 500);
	glutCreateWindow("Spinning Square");
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutTimerFunc(100, timer, 0);
	glutMouseFunc(mouse);
	glutMainLoop();
}//end int main(int argc, char const *argv[])
	
