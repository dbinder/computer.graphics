* #Computer Graphics with Opengl.

* __1:__ List the operating characteristics for the following display technologies: raster refresh systems, vector refresh systems, plasma panels, and LCDs.
* __2:__ List some applications appropriate for each of the display technologies in Exercise 2-1

* __3:__ Determine the resolution (pixels per centimeter) in the x and y directions for the video monitor in use on your system.  Determine the aspect ratio, and explain how relative proportions of objects can be maintained on your system. 

* __4:__ Consider three different raster systems with resolutions of 640 by 480, 1280 by 1024, and 2560 by 2048. What frame buffer (in bytes) is needed for each of these systems to store 12 bits per pixel? How much storage is required for each system if 24 bits per pixel are to be stored?

* __5:__ Suppose RGB Raster system is to be designed using an 8 inch by 10 inch screen with a resolution of 100 pixels per inch in each direction if we want to store 6 bits per pixel in the frame buffer, how much storage(in bytes) do we need for the frame buffer?

* __6:__ How long would it take to load a 640-by-480 frame buffer with 12 bits per pixel if 10^5 bits can be transferred per second? How long would it take to load a 24-bit-per-pixel frame buffer with a resolution of 1280 by 1024 using the same transfer rate?

* __7:__ Supposed we have a computer with 32 bits per word and a transfer rate of 1 mip(1 million instructions per second) How long would it take to fill the frame buffer of a 300 dpi(dots per inch) laser printer with a page size of 8.5 inches by 11 inches?

* __8:__ Consider two raster systems with resolutions of 640 by 480 and 1280 by 1024. How many pixels could be accessed per second in each of these systems by a display controller that refreshes the screen at a rate of 60 frames per second? what is the access time per pixel in each system?

* __9:__ Suppose we have a video monitor with a display area that measures 12 inches across and 9.6 inches high. If the resolution is 1280 by 1024 and the aspect ratio is 1, what  is the diameter of each screen point?
* __10:__ How much time is spent scanning across each row of pixels during screen refresh on a raster system with a resolution of 1280 by 1024 and a refresh rate of 60 frames per second. 
* __11:__ Consider a non-interlaced raster monitor with a resolution of n by m (m scans lines and n pixels per scan line), a refresh rate of r frames per second, a horizontal retrace time if theorize, and a vertical retrace time of tvert. What is the fraction of the total refresh time per frame spent in retrace of the electron beam?
* __12:__ What is the fraction of the total refresh time per frame spend in retrace of the electron beam for a non-interlaced raster system with a resolution of 1280 by 1024, a refresh rate of 60 hz, a horizontal  retrace time of 5 microseconds, and a vertical retrace time of 500 microseconds?  
* __13:__  Assuming that a certain full-color (24-bit-per-pixel) RGB raster system has a 512-by-512 frame buffer, how many distinct color choices (intensity levels) would we have available? How many different colors could we display at any one time?
* __14:__ Compare the advantages and disadvantages of a three-dimensional monitor using a trifocals mirror to those of a stereoscopic system. 
* __15:__ List the different input and output components that are typically used with virtual-reality systems. Also, explain how users interact with a virtual scene displayed with different output devices, such as two-dimensional and stereoscopic monitors.
* __16:__ Explain how a virtual-reality systems can be used in design applications. What are some other applications for virtual-reality systems?
* __17:__ List some applications for large-screen displays.
* __18:__ Explain the differences between a general graphics systems designed for a programmer and one designed for a specific application, such as architectural design.
* __19:__ Explain the differences between the OpenGl core library, the OpenGl Utility, and the OpenGL Utility Toolkit.
* __20__ What command could we used to set the color of an OpenGl display window to light gray? What command would we use to set the color of the display window to black>?
* __21:__ List the statements needed to set up an OpenGL display window whose lower-right corner is at pixel position (200,200), with a window width of 100 pixels and a height of 75 pixels. 
* __22:__ Explain what is meant by the term "OpenGL display callback function". 