INCLUDE = -I/usr/X11R6/include/
LIBDIR  = -L/usr/X11R6/lib

FLAGS = -Wall
CPP = g++
CC = gcc                                  
CFLAGS = $(FLAGS) $(INCLUDE)
LIBS =  -lglut -lGL -lGLU -lGLEW -lm

All: triangle                            

triangle: triangle.o
	$(CPP) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBS) 

tetrahedron: tetrahedron.o
	$(CPP) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBS) 

torus: torus.o
	$(CPP) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBS) 

sierpinski.gasket: sierpinski.gasket.o
	$(CPP) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBS) 

sierpinski.3d: sierpinski.3d.o
	$(CPP) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBS) 

spinning.sqaure: spinning.square.o
	$(CPP) $(CFLAGS) -o $@ $(LIBDIR) $< $(LIBS) 

clean: 
	rm -rf *o triangle tetrahedron torus sierpinski.gasket sierpinski.3d spinning.sqaure
